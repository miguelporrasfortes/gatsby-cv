import React from "react"
import IcGitLab from "../../../images/icons/web.svg"
import IcLinkedin from "../../../images/icons/linkedin.svg"
import "./Social.scss"

const socialMedia = [
  {
    icon: <IcGitLab />,
    link: "https://gitlab.com/miguelporrasfortes",
  },
  {
    icon: <IcLinkedin />,
    link: "https://www.linkedin.com/in/miguel-porras-fortes/",
  },
]

export default function Social() {
  return (
    <div className="social">
      {socialMedia.map((social, index) => (
        <a href={social.link} target="_blank" rel="noreferrer noopener">
          {social.icon}
        </a>
      ))}
    </div>
  )
}
