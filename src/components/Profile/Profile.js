import React from "react"
import { Container, Row, Col, Image } from "react-bootstrap"
import Social from "./Social"
import profileImage from "../../images/recortada.jpg"
import "./Profile.scss"

const data = [
  {
    title: "Date Of Birth:",
    info: "21.08.1989",
  },
  {
    title: "Address:",
    info: "Muñoz León 7, 41009",
  },
  {
    title: "E-mail:",
    info: "miguel.porras.fortes@gmail.com",
  },
  {
    title: "Telephone:",
    info: "+34 636 23 91 77",
  },
]

export default function Profile() {
  return (
    <div className="profile">
      <div className="wallpaper" />
      <div className="dark" />
      <Container className="box">
        <Row className="info">
          <Col xs={12} md={4}>
            <Image src={profileImage} fluid />
          </Col>
          <Col xs={12} md={8} className="info__data">
            <span>Info</span>
            <p>Miguel Porras Fortes</p>
            <p>Full Stack Developer</p>
            <hr />
            <div className="more-info">
              {data.map((item, index) => (
                <div key={index} className="item">
                  <p>{item.title}</p>
                  <p>{item.info}</p>
                </div>
              ))}
            </div>
          </Col>
        </Row>
        <Social />
      </Container>
    </div>
  )
}
