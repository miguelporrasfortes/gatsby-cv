import React from "react"
import { Container, Button } from "react-bootstrap"
import CV from "../../images/cv-mpf.pdf"
import "./AboutMe.scss"

export default function AboutMe() {
  return (
    <Container className="about-me">
      <p>
        Coding bootcamp graduate, with a focus on web and mobile development
        under LAMP​ and ​ MERN​ stacks. Willing to contribute my strong passion
        for learning, my international experience in the ​ petrochemical
        sector,​ and my scientific background as a ​ master in Chemistry​ to a
        thrilling, innovative and challenging project.
      </p>
      <hr />
      <a href={CV} target="_blank" rel="noreferrer">
        <Button primary="true">Download CV</Button>
      </a>
    </Container>
  )
}
