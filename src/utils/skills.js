export const frontendSkills = [
  { type: "JavaScript", level: 65 },
  { type: "React JS", level: 55 },
  { type: "Angular JS", level: 55 },
  { type: "Gatsby", level: 60 },
  { type: "HTML5", level: 65 },
  { type: "CSS3", level: 65 },
  { type: "SASS", level: 60 },
  { type: "Bootstrap", level: 60 },
]

export const frontendSkillsColors = {
  bar: "#3498db",
  title: {
    text: "#fff",
    background: "#2980b9",
  },
}

export const backendSkills = [
  { type: "Node JS", level: 60 },
  { type: "Express JS", level: 55 },
  { type: "PHP", level: 60 },
  { type: "Java", level: 55 },
  { type: "Springboot", level: 50 },
]

export const backendSkillsColors = {
  bar: "#00bd3f",
  title: {
    text: "#fff",
    background: "#009331",
  },
}

export const soSkills = [
  { type: "Git", level: 60 },
  { type: "Docker", level: 40 },
  { type: "Spanish", level: 100 },
  { type: "English", level: 80 },
  { type: "German", level: 80 },
  { type: "Portuguese", level: 70 },
]

export const soSkillsColors = {
  bar: "#f09c00",
  title: {
    text: "#fff",
    background: "b46900",
  },
}
