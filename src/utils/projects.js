import maleteoSymfony from "../images/projects/maleteo-symfony.png"
import malsters from "../images/projects/LOGO MALSTERS.svg"
import maleteo from "../images/projects/maleteo-landing.png"
import curriculum from "../images/projects/gatsby.png"
import filmyk from "../images/Captura de pantalla de 2020-11-30 19-12-11.png"
import unijava from "../images/UniversidadJava.png"

export default [
  {
    title: "Filmyk Blog",
    description:
      "Filmyk blog, with responsive design featuring SASS, programmed in React Gatsby, including GraphQL, Strapi, Heroku and Netlify.",
    url: "https://filmyk-blog.netlify.app/",
    image: filmyk,
  },
  {
    title: "Maleteo Symfony",
    description:
      "CRUD App programmed in PHP Symfony featuring three Docker Containers as environment.",
    url: "https://gitlab.com/miguelporrasfortes/maleteo-symfony",
    image: maleteoSymfony,
  },
  {
    title: "Malsters",
    description: "CRUD App programmed in JS MERN Stack to foster beer culture.",
    url: "https://gitlab.com/miguelporrasfortes/malsters-app-back",
    image: malsters,
  },
  {
    title: "Maleteo Landing Page",
    description:
      "Landing page in responsive design for the Maleteo App in CSS3 and HTML 5",
    url: "https://gitlab.com/miguelporrasfortes/maleteo_landing_page",
    image: maleteo,
  },
  {
    title: "Gatsby Online CV",
    description:
      "Online CV programmed in Gatsby JS, featuring React Bootstrap and SASS",
    url: "https://gitlab.com/miguelporrasfortes/maleteo_landing_page",
    image: curriculum,
  },
  {
    title: "Java Course",
    description:
      "Projects with regard to the Java Course https://www.udemy.com/course/universidad-java-especialista-en-java-desde-cero-a-master/ including Java, POO, JDBC, Servlets, JavaEE, Web Services, JSF, EJB, JPA, PrimeFaces, Hibernate, Spring and Struts",
    url: "https://gitlab.com/miguelporrasfortes/",
    image: unijava,
  },
]
